import React from 'react';

const Footer = () => {
	return(
		<React.Fragment>
			<div className="footer fixed-bottom">
				<p className="footer-text">All assets are not owned by me and is used for education purposes only.</p>
			</div>
		</React.Fragment>
	)
}

export default Footer;