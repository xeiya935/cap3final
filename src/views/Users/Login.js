import React, { useState } from "react";
import { FormInput } from "../../globalcomponents";
import { Button } from "reactstrap";
import axios from "axios";
import {NavigationBar, Footer} from '../Layout'

const Login = () => {
  
    const [email,setEmail] = useState("");
    const [password,setPassword] = useState("");

    const handleEmailChange = (e) =>{
        setEmail(e.target.value)
        console.log(e.target.value)
    }

    const handlePasswordChange = (e) =>{
        setPassword(e.target.value)
        console.log(e.target.value)
    }

    const handleLogin = () => {
        axios.post('https://warm-everglades-62190.herokuapp.com/login',{
            email,
            password
        }).then(res=>{
          // can only save string in storage
          sessionStorage.token = res.data.token;
          //JSON.stringify converts data into string
          sessionStorage.user = JSON.stringify(res.data.user);
          // for local storage
          // localStorage.token = res.data.token;
          console.log(res.data.token);

          window.location.replace('#/courses');
        });
    }

    const handleRedirectRegister = () => {
      window.location.replace('#/register');
    }

  return (
    <React.Fragment>
      <NavigationBar />
      <div className="login-container center flex-column">
        <h1 className="text-center pt-5 header-text">Login</h1>
        <div className="col-lg-4 offset-lg-4 pb-3 col-md-4 offset-md-4 form-container">
          <FormInput
            label={"Email"}
            placeholder={"Enter Your Email"}
            type={"email"}
            onChange={handleEmailChange}
          />
          <FormInput
            label={"Password"}
            placeholder={"Enter Your Password"}
            type={"password"}
            onChange={handlePasswordChange}
          />
          <Button 
              block 
              // color="success"
              className="success"
              onClick={handleLogin}
              >
            Login
          </Button>
          <p className="card-text">Not yet a registered user? 
            <button 
              className="register-button"
              onClick={handleRedirectRegister}
            >Click here!</button>
          </p>
        </div>
      </div>
      <Footer />
    </React.Fragment>
  );
};

export default Login;