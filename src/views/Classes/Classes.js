import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {ClassRow, UpdateBooking} from './components'
import {NavigationBar, Footer} from '../Layout';
import {CSVLink} from 'react-csv';

const Classes = () => {

	const [classes, setClasses] = useState([]);
	const [classSchedule, setClassSchedule] = useState("");
	const [showUpdateForm, setShowUpdateForm] = useState(false);
	const [date, setDate] = useState("");
	const [time, setTime] = useState("");
	const csvData = classes
	const headers = [
	{label: "Class", key: "language"},
	{label: "Time", key: "time"},
	{label: "Date", key: "date"},
	{label: "Tutor", key: "tutor"},
	{label: "Status", key: "bookingStatus"}
	]

	const handleShowUpdateForm = () => {
		setShowUpdateForm(!showUpdateForm)
	}

	const handleSetClassSchedule = (classSchedule) => {
		setClassSchedule(classSchedule);
		handleShowUpdateForm();
		console.log(classSchedule)
		setDate(classSchedule.date)
		setTime(classSchedule.time)
	}

	const handleDateChange = (selectedDay) => {
		setDate(selectedDay)
		console.log(selectedDay)
	}

	const handleTimeChange = (time) => {
		setTime(time)
		console.log(time)
	}

	const handleUpdateBooking = () => {
		axios.patch('https://warm-everglades-62190.herokuapp.com/adminUpdateClass/'+ classSchedule._id, {
			time: time,
			date: date
		}).then(res=>{
			let index = classes.findIndex(classId=>classId._id === classSchedule._id)
			let newClasses = [...classes];
			newClasses.splice(index,1,res.data)
			setClasses(newClasses);
			handleRefresh();
			handleShowUpdateForm();
		})
	}

	const handleRefresh = () => {
		setDate("");
		setTime("");
		setClassSchedule("");
	}

	const handleDeleteClass = (classId) => {
		axios.delete('https://warm-everglades-62190.herokuapp.com/deleteClass/'+classId).then(res=>{
			let index = classes.findIndex(classSchedule=>classSchedule._id===classId);
			let newClasses = [...classes];
			newClasses.splice(index,1);
			setClasses(newClasses);
		})
	}

	const [user, setUser] = useState("");

	useEffect(()=>{

		if(sessionStorage.token){
			let loggedInUser = JSON.parse(sessionStorage.user)
			setUser(loggedInUser)
			console.log(loggedInUser)
			if(loggedInUser.role === "Super Admin" || loggedInUser.role === "Admin"){
				axios.get('https://warm-everglades-62190.herokuapp.com/showClasses').then(res=>{
					setClasses(res.data);
				})
			}else{
				let student = loggedInUser.firstName + " " + loggedInUser.lastName
				console.log(student)
				axios.get('https://warm-everglades-62190.herokuapp.com/showClassesByUser/'+student).then(res=>{
					setClasses(res.data)
					console.log(res.data)
				})
			}

		}else{
			window.location.replace('#/login')
		}

	}, []);

	return(
		<React.Fragment>
			<NavigationBar />
				<div className="classes-container">
					<h1 className="header-text">My Classes</h1>
					<div className="d-flex justify-content-center mb-3">
						<CSVLink data={csvData} headers={headers} className="success">Print Schedules</CSVLink>	
					</div>
					<UpdateBooking 
						showUpdateForm={showUpdateForm}
						handleShowUpdateForm={handleShowUpdateForm}
						handleRefresh={handleRefresh}
						handleDateChange={handleDateChange}
						handleTimeChange={handleTimeChange}
						classSchedule={classSchedule}
						handleUpdateBooking={handleUpdateBooking}
					/>
					<div className="col-lg-10 offset-lg-1">
						<table className="table-custom">
							<thead className="table-head">
								<tr>
									<th style={user.role === "Super Admin" || user.role === "Admin" ? {display:"block"} : {display:"none"}}>ClassCode</th>
									<th>Class</th>
									<th>Date</th>
									<th>Time</th>
									<th>Tutor</th>
									<th style={user.role === "Super Admin" || user.role === "Admin" ? {display:"block"} : {display:"none"}}>Student</th>
									<th>Status</th>
									<th style={user.role === "Super Admin" || user.role === "Admin" ? {display:"block"} : {display:"none"}}>Action</th>
								</tr>
							</thead>
							<tbody>
								{classes.map(classSchedule=>
									<ClassRow 
										key={classSchedule._id}
										classSchedule={classSchedule}
										handleDeleteClass={handleDeleteClass}
										handleShowUpdateForm={handleShowUpdateForm}
										handleSetClassSchedule={handleSetClassSchedule}
										user={user}
									/>
								)}
							</tbody>
						</table>
					</div>
				</div>
			<Footer />
		</React.Fragment>
	)
}

export default Classes;