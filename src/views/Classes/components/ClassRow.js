import React from 'react';
import {Button} from 'reactstrap';

const ClassRow = (props) => {

	const classSchedule = props.classSchedule;
	const user = props.user;

	return (
		<React.Fragment>
			<tr>
				<td>{classSchedule.classCode}</td>
				<td>{classSchedule.language}</td>
				<td>{classSchedule.date}</td>
				<td>{classSchedule.time}</td>
				<td>{classSchedule.tutor}</td>
				<td>{classSchedule.student}</td>
				<td>{classSchedule.bookingStatus}</td>
				<td>
					<Button
						// color="info"
						className="info mx-1"
						onClick={()=>props.handleSetClassSchedule(classSchedule)}
					>Update Class</Button>
					<Button
						// color="danger"
						className="danger mx-1"
						onClick={()=>props.handleDeleteClass(classSchedule._id)}
					>Delete Class</Button>
				</td>
				
			</tr>
		</React.Fragment>
	)
}

export default ClassRow;