import React, {useState, useEffect} from 'react';
import {Modal, ModalHeader, ModalBody, Dropdown, DropdownToggle, DropdownMenu} from 'reactstrap';
import axios from 'axios';
import {Calendar, Stripe} from '../../../globalcomponents';
import TimeSelection from './TimeSelection';
import PaymentConfirmation from './PaymentConfirmation'

const BookingForm = (props) => {

	const [times, setTimes] = useState([])
	const [dropdownOpen, setDropdownOpen] = useState(false)

	const [showPaymentConfirmation, setShowPaymentConfirmation] = useState(false);

	const handleShowPaymentConfirmation = () => {
		setShowPaymentConfirmation(!showPaymentConfirmation);
	}

	const toggle = () => {
		setDropdownOpen(!dropdownOpen)
	}

	useEffect(()=>{
		axios.get('https://warm-everglades-62190.herokuapp.com/admin/showTimes').then(res=>{
			setTimes(res.data)
			console.log(res.data)
		})
	}, []);

	return(
		<React.Fragment>
			<Modal
				isOpen={props.showBookingForm}
				toggle={props.handleCancelBooking}
			>
				<ModalHeader
					toggle={props.handleCancelBooking}

				>
					Book a Class
				</ModalHeader>
				<ModalBody
					className="d-flex justify-content-center align-items-center flex-column"
				>
			        <h1 className="modal-text">{props.course.language}</h1>
			        <Calendar 
			        	handleDateChange={props.handleDateChange}
			        />
			        <Dropdown
						isOpen={dropdownOpen}
						toggle={toggle}
						className="pb-3"
					>
						<DropdownToggle caret className="dropdown-size">
							{props.time === "" ? "Time Slot" : props.time}
						</DropdownToggle>
						<DropdownMenu>
							{times.map(time=>
								<TimeSelection
									key={time._id}
									time={time}
									handleTimeChange={props.handleTimeChange}
								/>
							)}
						</DropdownMenu>
					</Dropdown>
					<Stripe 
						course={props.course}
						time={props.time}
						date={props.date}
						handleCancelBooking={props.handleCancelBooking}
						handleAddBooking={props.handleAddBooking}
						student={props.student}
						handleShowPaymentConfirmation={handleShowPaymentConfirmation}
					/>
					<PaymentConfirmation 
						showPaymentConfirmation={showPaymentConfirmation}
						handleShowPaymentConfirmation={handleShowPaymentConfirmation}
						handleShowBookingForm={props.handleShowBookingForm}
					/>
				</ModalBody>
			</Modal>
		</React.Fragment>
	)
}

export default BookingForm;