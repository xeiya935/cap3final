import React from 'react';
import {Modal, ModalHeader, ModalBody, Button} from 'reactstrap';

const PaymentConfirmation = (props) => {

	const handlePaymentConfirmation = () => {
		props.handleShowPaymentConfirmation()
		props.handleShowBookingForm()
	}

	return (
		<Modal
			isOpen={props.showPaymentConfirmation}
			toggle={handlePaymentConfirmation}
		>
			<ModalHeader
				toggle={handlePaymentConfirmation}
			>
				Payment SuccessFul!!
			</ModalHeader>
			<ModalBody>
				<div className="d-flex justify-content-center">
					<Button
						className="success"
						onClick={handlePaymentConfirmation}
					>Confirm</Button>
				</div>
			</ModalBody>
		</Modal>
	)
}

export default PaymentConfirmation;