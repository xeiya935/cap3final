import CourseCard from './CourseCard';
import BookingForm from './BookingForm'

export {
	CourseCard,
	BookingForm
}