import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CardDetails from './CardDetails';

const Stripe = (props) => {
	return(
		<React.Fragment>
			<StripeProvider
				apiKey="pk_test_lqz2UfRpHYPnwKg9hQ80KL0B00d0N9b21f"
			>
			<div className="payment-container">
				<h3 className="text-center py-1 modal-text">Payment Details</h3>
				<div className="d-flex justify-content-center">
					<Elements>
						<CardDetails 
							course={props.course}
							time={props.time}
							date={props.date}
							handleCancelBooking={props.handleCancelBooking}
							handleAddBooking={props.handleAddBooking}
							student={props.student}
							handleShowPaymentConfirmation={props.handleShowPaymentConfirmation}
						/>
					</Elements>
				</div>
			</div>
			</StripeProvider>
		</React.Fragment>
	)
}

export default Stripe;