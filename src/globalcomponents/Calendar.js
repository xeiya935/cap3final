import React, {useState} from 'react';
import DayPicker from 'react-day-picker';
import moment from 'moment';

const Calendar = (props) => {

	const [year, setYear] = useState(0);
	const [month, setMonth] = useState(0);
	const [day, setDay] = useState(0)

	const handleDayClick = (day, modifiers={}) => {
		if (modifiers.disabled) {
	    	return;
	    }
		props.handleDateChange(moment(day).format("MMM DD YYYY"))
		setYear(moment(day).format("YYYY"))
		setMonth(moment(day).format("MM"))
		setDay(moment(day).format("DD"))
	}

	// for disabling certain days of the week
	// const disabledDays = {
	// 	daysOfWeek: [0,6]
	// }

	// for disabling all past dates
	const past = {
		before: new Date()
	}

	// const today = new Date()

	let selectedDay = new Date(year, month-1, day)

	return(
		<React.Fragment>
			<div className="d-flex justify-content-center">
				<DayPicker 
					onDayClick={handleDayClick}
					showOutsideDays
					disabledDays={[past, new Date()]}
					selectedDays={selectedDay}
				/>
			</div>
		</React.Fragment>
	)
}

export default Calendar;